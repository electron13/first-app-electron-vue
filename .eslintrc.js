module.exports = {
  root: true,
  globals: {
    __static: 'readonly',
  },
  env:{
    "node": true
  },
  extends: [
    "plugin:vue/vue3-recommended",
  ],
  parserOptions: {
    "parser": "babel-eslint"
  },
  rules: {}
}